class Waggon:

    ## usual init, might be overkill but better to be safe!
    def __init__(self,length,empty_weigth,max_weight,owner):
        self.length=length
        self.empty_weigth = empty_weigth
        self.weight=empty_weigth
        self.max_weight = max_weight
        self.owner= owner

    ## define beladen method, returns an int/float! (needs to be converted into string if necessary)
    def beladen(self,weight):
        if weight+ self.weight > self.max_weight:
            self.weight = self.max_weight
        else:
            self.weight = self.weight + weight

    def __repr__(self):
        return "Betreiber {}, length {}m, current weight {} ".format(self.owner,self.length,self.weight)

class Locomotive:

    def __init__(self,length,weight,max_pullweight,typ):
        self.length = length
        self.weight = weight
        self.max_pullweight = max_pullweight
        self.typ = typ

    def __repr__(self):
        return "Locomitive Type '{}' (maximal pullingforce {}t)".format(self.typ,self.max_pullweight)
    ## locomotive has no cool functionality yet! :(


class Train:

    def __init__(self,loc,id):
        assert isinstance(loc,Locomotive) ## this requires you to insert a real loc. Otherwise it will fail!
        ## ensures that variables like "loc.max_pullweight" are defined
        self.length = loc.length
        self.loc= loc
        self.max_pullweight = loc.max_pullweight
        self.max_weight = 0
        self.waggons = [] ## creats an empty array/list of all waggons
        self.weight = loc.weight

    def anhaengen(self,wagon):
        assert isinstance(wagon,Waggon) # requires you to insert a real wagon! Same as above
        ## thinking about edge cases
        if self.max_weight+ wagon.max_weight> self.max_pullweight:
            print('you cannot add this wagon to the train. It is too heavy!')
            return False
        self.waggons.append(wagon)
        self.length += wagon.length ## this is equal to self.length = self.length + wagon.length
        self.max_weight += wagon.max_weight
        #### aufgabenstellung erlaubt diese Idee nicht!
        ##  self.weight += wagon.weight ## this is equal to self.weight = self.weight + wagon.weight
        
        ####
        
        return True

    ## currently unfunctional repr, ISNT NEEDED per your assignment, or not explicitely mentioned
    def __repr__(self):
        return ('current weight:{}, '.format(self.calcweight()))


    ## necessary function per you assignment
    def wagenreihung(self):
        for waggon in self.waggons:
            print(waggon)
    
    ## calculates the current weight of the train
    def calcweight(self):
        current_weight=self.loc.weight
        for waggon in self.waggons:
            current_weight += waggon.weight
        return current_weight


    ##creates tuple of length, max_pullweight, currentweight
    def gesamtdaten(self):
        return (self.length,self.max_pullweight,self.calcweight())
        
    ## mrints the tuple above!
    def printGesamtdaten(self):
        l,alloww,cweigh=self.gesamtdaten()
        print("total length {}, allowed maxium weight {}, current weight {}".format(l,alloww,cweigh))



## creates a train from a loc, a list of waggons, and a name/id

## prints the length, max weight and the current weight twice,
## once before loading and once after loading
def createTrain(loc, waggons, name):
    train = Train(loc,name)
    for waggon in waggons:
        train.anhaengen(waggon)
    print(train)
    
    train.printGesamtdaten()
    n=0
    for wagon in train.waggons:
        n += 1
        wagon.beladen((0.8+0.01* (n%4))* wagon.max_weight- wagon.empty_weigth )
    train.printGesamtdaten()
    return train

myloc = Locomotive(20,180,2000,'superlocomotive')
mywaggon1 = Waggon(25,40,100,'mathias')
mywaggon2 = Waggon(30,50,150,'dieter')
mytrain = Train(myloc,'meinzug')


while True:
    print('x')
    
    if not (mytrain.anhaengen(mywaggon1)):
        print(mytrain)
        break

    
#createTrain(myloc,[mywaggon1,mywaggon1,mywaggon1,mywaggon2,mywaggon2,mywaggon2],'testzug')
